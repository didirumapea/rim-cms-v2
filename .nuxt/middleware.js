const middleware = {}

middleware['redirect'] = require('../middleware/redirect.js')
middleware['redirect'] = middleware['redirect'].default || middleware['redirect']

middleware['router-auth'] = require('../middleware/router-auth.js')
middleware['router-auth'] = middleware['router-auth'].default || middleware['router-auth']

export default middleware
