import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _0e34f98f = () => interopDefault(import('../pages/login_page.vue' /* webpackChunkName: "pages/login_page" */))
const _9eddcc0e = () => interopDefault(import('../pages/components/accordion.vue' /* webpackChunkName: "pages/components/accordion" */))
const _7caeaee6 = () => interopDefault(import('../pages/components/alert.vue' /* webpackChunkName: "pages/components/alert" */))
const _96b414e4 = () => interopDefault(import('../pages/components/animations.vue' /* webpackChunkName: "pages/components/animations" */))
const _eb4eafaa = () => interopDefault(import('../pages/components/avatars.vue' /* webpackChunkName: "pages/components/avatars" */))
const _98c8bc6e = () => interopDefault(import('../pages/components/badge_label.vue' /* webpackChunkName: "pages/components/badge_label" */))
const _79183730 = () => interopDefault(import('../pages/components/base.vue' /* webpackChunkName: "pages/components/base" */))
const _1d0d6622 = () => interopDefault(import('../pages/components/breadcrumb.vue' /* webpackChunkName: "pages/components/breadcrumb" */))
const _09cc01f2 = () => interopDefault(import('../pages/components/buttons.vue' /* webpackChunkName: "pages/components/buttons" */))
const _0b43ab14 = () => interopDefault(import('../pages/components/cards.vue' /* webpackChunkName: "pages/components/cards" */))
const _1823f650 = () => interopDefault(import('../pages/components/color_palette.vue' /* webpackChunkName: "pages/components/color_palette" */))
const _bca2175e = () => interopDefault(import('../pages/components/drop_dropdowns.vue' /* webpackChunkName: "pages/components/drop_dropdowns" */))
const _5760c43a = () => interopDefault(import('../pages/components/fab_buttons.vue' /* webpackChunkName: "pages/components/fab_buttons" */))
const _3d9f196c = () => interopDefault(import('../pages/components/filters.vue' /* webpackChunkName: "pages/components/filters" */))
const _5845fb3a = () => interopDefault(import('../pages/components/footer.vue' /* webpackChunkName: "pages/components/footer" */))
const _1aad0db6 = () => interopDefault(import('../pages/components/grid.vue' /* webpackChunkName: "pages/components/grid" */))
const _5c6f30c6 = () => interopDefault(import('../pages/components/height.vue' /* webpackChunkName: "pages/components/height" */))
const _7203396a = () => interopDefault(import('../pages/components/icons.vue' /* webpackChunkName: "pages/components/icons" */))
const _5e2e8374 = () => interopDefault(import('../pages/components/lists.vue' /* webpackChunkName: "pages/components/lists" */))
const _df0b8294 = () => interopDefault(import('../pages/components/masonry.vue' /* webpackChunkName: "pages/components/masonry" */))
const _6fecf91e = () => interopDefault(import('../pages/components/modals_dialogs.vue' /* webpackChunkName: "pages/components/modals_dialogs" */))
const _5c0d35ce = () => interopDefault(import('../pages/components/notifications.vue' /* webpackChunkName: "pages/components/notifications" */))
const _1263ca39 = () => interopDefault(import('../pages/components/pagination.vue' /* webpackChunkName: "pages/components/pagination" */))
const _4e68a606 = () => interopDefault(import('../pages/components/progress_spinners.vue' /* webpackChunkName: "pages/components/progress_spinners" */))
const _a07fdc74 = () => interopDefault(import('../pages/components/scrollable.vue' /* webpackChunkName: "pages/components/scrollable" */))
const _4588ab80 = () => interopDefault(import('../pages/components/slider.vue' /* webpackChunkName: "pages/components/slider" */))
const _1a6a5557 = () => interopDefault(import('../pages/components/sortable.vue' /* webpackChunkName: "pages/components/sortable" */))
const _aa0de1f8 = () => interopDefault(import('../pages/components/tables.vue' /* webpackChunkName: "pages/components/tables" */))
const _aa9f7546 = () => interopDefault(import('../pages/components/tabs.vue' /* webpackChunkName: "pages/components/tabs" */))
const _1bb05120 = () => interopDefault(import('../pages/components/timeline.vue' /* webpackChunkName: "pages/components/timeline" */))
const _444b7e28 = () => interopDefault(import('../pages/components/toolbar.vue' /* webpackChunkName: "pages/components/toolbar" */))
const _5a9263a2 = () => interopDefault(import('../pages/components/tooltips.vue' /* webpackChunkName: "pages/components/tooltips" */))
const _81db75a2 = () => interopDefault(import('../pages/components/transitions.vue' /* webpackChunkName: "pages/components/transitions" */))
const _15c107f7 = () => interopDefault(import('../pages/components/width.vue' /* webpackChunkName: "pages/components/width" */))
const _d1bb7a80 = () => interopDefault(import('../pages/dashboard/v1.vue' /* webpackChunkName: "pages/dashboard/v1" */))
const _d19f4b7e = () => interopDefault(import('../pages/dashboard/v2.vue' /* webpackChunkName: "pages/dashboard/v2" */))
const _34373403 = () => interopDefault(import('../pages/forms/dynamic_fields.vue' /* webpackChunkName: "pages/forms/dynamic_fields" */))
const _376a0284 = () => interopDefault(import('../pages/forms/regular_elements.vue' /* webpackChunkName: "pages/forms/regular_elements" */))
const _3bacdec3 = () => interopDefault(import('../pages/forms/validation.vue' /* webpackChunkName: "pages/forms/validation" */))
const _5acafbb5 = () => interopDefault(import('../pages/forms/wizard.vue' /* webpackChunkName: "pages/forms/wizard" */))
const _4fad8f0b = () => interopDefault(import('../pages/forms/wizard/step1.vue' /* webpackChunkName: "pages/forms/wizard/step1" */))
const _4fbba68c = () => interopDefault(import('../pages/forms/wizard/step2.vue' /* webpackChunkName: "pages/forms/wizard/step2" */))
const _4fc9be0d = () => interopDefault(import('../pages/forms/wizard/step3.vue' /* webpackChunkName: "pages/forms/wizard/step3" */))
const _1c7af062 = () => interopDefault(import('../pages/pages/blank.vue' /* webpackChunkName: "pages/pages/blank" */))
const _0efc43bb = () => interopDefault(import('../pages/pages/blank_header_expanded.vue' /* webpackChunkName: "pages/pages/blank_header_expanded" */))
const _51fc03ad = () => interopDefault(import('../pages/pages/chat.vue' /* webpackChunkName: "pages/pages/chat" */))
const _3f149b9c = () => interopDefault(import('../pages/pages/contact_list.vue' /* webpackChunkName: "pages/pages/contact_list" */))
const _4b6970b6 = () => interopDefault(import('../pages/pages/contact_list_single.vue' /* webpackChunkName: "pages/pages/contact_list_single" */))
const _8b275be6 = () => interopDefault(import('../pages/pages/gallery.vue' /* webpackChunkName: "pages/pages/gallery" */))
const _2ffd6aed = () => interopDefault(import('../pages/pages/help_faq.vue' /* webpackChunkName: "pages/pages/help_faq" */))
const _3d48148a = () => interopDefault(import('../pages/pages/invoices.vue' /* webpackChunkName: "pages/pages/invoices" */))
const _3fca03fe = () => interopDefault(import('../pages/pages/invoices/index.vue' /* webpackChunkName: "pages/pages/invoices/index" */))
const _55362434 = () => interopDefault(import('../pages/pages/invoices/_id.vue' /* webpackChunkName: "pages/pages/invoices/_id" */))
const _88023262 = () => interopDefault(import('../pages/pages/issues.vue' /* webpackChunkName: "pages/pages/issues" */))
const _058e1d82 = () => interopDefault(import('../pages/pages/issues/details.vue' /* webpackChunkName: "pages/pages/issues/details" */))
const _7dc04ba6 = () => interopDefault(import('../pages/pages/issues/details/_id.vue' /* webpackChunkName: "pages/pages/issues/details/_id" */))
const _17a71b8e = () => interopDefault(import('../pages/pages/issues/list.vue' /* webpackChunkName: "pages/pages/issues/list" */))
const _51c83b22 = () => interopDefault(import('../pages/pages/mailbox.vue' /* webpackChunkName: "pages/pages/mailbox" */))
const _1aca99b2 = () => interopDefault(import('../pages/pages/mailbox/index.vue' /* webpackChunkName: "pages/pages/mailbox/index" */))
const _4f23d072 = () => interopDefault(import('../pages/pages/mailbox/compose.vue' /* webpackChunkName: "pages/pages/mailbox/compose" */))
const _504242dc = () => interopDefault(import('../pages/pages/mailbox/message/_id.vue' /* webpackChunkName: "pages/pages/mailbox/message/_id" */))
const _0a727288 = () => interopDefault(import('../pages/pages/notes.vue' /* webpackChunkName: "pages/pages/notes" */))
const _2a27544a = () => interopDefault(import('../pages/pages/poi_listing.vue' /* webpackChunkName: "pages/pages/poi_listing" */))
const _3a2d53da = () => interopDefault(import('../pages/pages/pricing_tables.vue' /* webpackChunkName: "pages/pages/pricing_tables" */))
const _3853a878 = () => interopDefault(import('../pages/pages/settings.vue' /* webpackChunkName: "pages/pages/settings" */))
const _5c5a977e = () => interopDefault(import('../pages/pages/task_board.vue' /* webpackChunkName: "pages/pages/task_board" */))
const _939763ec = () => interopDefault(import('../pages/pages/user_profile.vue' /* webpackChunkName: "pages/pages/user_profile" */))
const _95553cce = () => interopDefault(import('../pages/plugins/ajax.vue' /* webpackChunkName: "pages/plugins/ajax" */))
const _5b44d992 = () => interopDefault(import('../pages/plugins/calendar.vue' /* webpackChunkName: "pages/plugins/calendar" */))
const _a15e05e4 = () => interopDefault(import('../pages/plugins/charts.vue' /* webpackChunkName: "pages/plugins/charts" */))
const _18fddb16 = () => interopDefault(import('../pages/plugins/code_editor.vue' /* webpackChunkName: "pages/plugins/code_editor" */))
const _3c3f4392 = () => interopDefault(import('../pages/plugins/data_grid.vue' /* webpackChunkName: "pages/plugins/data_grid" */))
const _021539c8 = () => interopDefault(import('../pages/plugins/datatables.vue' /* webpackChunkName: "pages/plugins/datatables" */))
const _65b77a2e = () => interopDefault(import('../pages/plugins/diff_tool.vue' /* webpackChunkName: "pages/plugins/diff_tool" */))
const _f84bad6c = () => interopDefault(import('../pages/plugins/gantt_chart.vue' /* webpackChunkName: "pages/plugins/gantt_chart" */))
const _2de83cf4 = () => interopDefault(import('../pages/plugins/google_maps.vue' /* webpackChunkName: "pages/plugins/google_maps" */))
const _4537836f = () => interopDefault(import('../pages/plugins/idle_timeout.vue' /* webpackChunkName: "pages/plugins/idle_timeout" */))
const _ee64f000 = () => interopDefault(import('../pages/plugins/image_cropper.vue' /* webpackChunkName: "pages/plugins/image_cropper" */))
const _78cac8bc = () => interopDefault(import('../pages/plugins/push_notifications.vue' /* webpackChunkName: "pages/plugins/push_notifications" */))
const _d551845e = () => interopDefault(import('../pages/plugins/tour.vue' /* webpackChunkName: "pages/plugins/tour" */))
const _18053417 = () => interopDefault(import('../pages/plugins/tree.vue' /* webpackChunkName: "pages/plugins/tree" */))
const _0938f5ac = () => interopDefault(import('../pages/plugins/vector_maps.vue' /* webpackChunkName: "pages/plugins/vector_maps" */))
const _05f6cc44 = () => interopDefault(import('../pages/plugins/vue_good_table.vue' /* webpackChunkName: "pages/plugins/vue_good_table" */))
const _3a80a3a4 = () => interopDefault(import('../pages/transaction/direct.vue' /* webpackChunkName: "pages/transaction/direct" */))
const _67aade4a = () => interopDefault(import('../pages/transaction/midtrans.vue' /* webpackChunkName: "pages/transaction/midtrans" */))
const _15b70bc4 = () => interopDefault(import('../pages/forms/advanced_elements/checkbox_radio.vue' /* webpackChunkName: "pages/forms/advanced_elements/checkbox_radio" */))
const _625ce1ef = () => interopDefault(import('../pages/forms/advanced_elements/color_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/color_picker" */))
const _344d2c4a = () => interopDefault(import('../pages/forms/advanced_elements/date_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/date_picker" */))
const _4a5f34e8 = () => interopDefault(import('../pages/forms/advanced_elements/date_range_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/date_range_picker" */))
const _06226d01 = () => interopDefault(import('../pages/forms/advanced_elements/inputmask.vue' /* webpackChunkName: "pages/forms/advanced_elements/inputmask" */))
const _76cd3300 = () => interopDefault(import('../pages/forms/advanced_elements/multiselect.vue' /* webpackChunkName: "pages/forms/advanced_elements/multiselect" */))
const _322c1aa8 = () => interopDefault(import('../pages/forms/advanced_elements/range_slider.vue' /* webpackChunkName: "pages/forms/advanced_elements/range_slider" */))
const _24c406e2 = () => interopDefault(import('../pages/forms/advanced_elements/rating.vue' /* webpackChunkName: "pages/forms/advanced_elements/rating" */))
const _68813ec1 = () => interopDefault(import('../pages/forms/advanced_elements/select2.vue' /* webpackChunkName: "pages/forms/advanced_elements/select2" */))
const _5a6d4b32 = () => interopDefault(import('../pages/forms/advanced_elements/switches.vue' /* webpackChunkName: "pages/forms/advanced_elements/switches" */))
const _5b5cefcb = () => interopDefault(import('../pages/forms/advanced_elements/time_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/time_picker" */))
const _8db79560 = () => interopDefault(import('../pages/forms/examples/advertising_evaluation_form.vue' /* webpackChunkName: "pages/forms/examples/advertising_evaluation_form" */))
const _12ad3fd4 = () => interopDefault(import('../pages/forms/examples/booking_form.vue' /* webpackChunkName: "pages/forms/examples/booking_form" */))
const _4450acd0 = () => interopDefault(import('../pages/forms/examples/car_rental_form.vue' /* webpackChunkName: "pages/forms/examples/car_rental_form" */))
const _3e51bd21 = () => interopDefault(import('../pages/forms/examples/checkout_form.vue' /* webpackChunkName: "pages/forms/examples/checkout_form" */))
const _dea8da58 = () => interopDefault(import('../pages/forms/examples/contact_forms.vue' /* webpackChunkName: "pages/forms/examples/contact_forms" */))
const _15ce5c41 = () => interopDefault(import('../pages/forms/examples/job_application_form.vue' /* webpackChunkName: "pages/forms/examples/job_application_form" */))
const _651a43ae = () => interopDefault(import('../pages/forms/examples/medical_history_form.vue' /* webpackChunkName: "pages/forms/examples/medical_history_form" */))
const _6d3cfd0e = () => interopDefault(import('../pages/forms/examples/registration_form.vue' /* webpackChunkName: "pages/forms/examples/registration_form" */))
const _1bdb94dc = () => interopDefault(import('../pages/forms/examples/rental_application_form.vue' /* webpackChunkName: "pages/forms/examples/rental_application_form" */))
const _91f096be = () => interopDefault(import('../pages/forms/examples/transaction_feedback_form.vue' /* webpackChunkName: "pages/forms/examples/transaction_feedback_form" */))
const _2fb6f93e = () => interopDefault(import('../pages/forms/wysiwyg/ckeditor.vue' /* webpackChunkName: "pages/forms/wysiwyg/ckeditor" */))
const _08a82029 = () => interopDefault(import('../pages/forms/wysiwyg/quill.vue' /* webpackChunkName: "pages/forms/wysiwyg/quill" */))
const _56974b58 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/login_page",
    component: _0e34f98f,
    name: "login_page"
  }, {
    path: "/components/accordion",
    component: _9eddcc0e,
    name: "components-accordion"
  }, {
    path: "/components/alert",
    component: _7caeaee6,
    name: "components-alert"
  }, {
    path: "/components/animations",
    component: _96b414e4,
    name: "components-animations"
  }, {
    path: "/components/avatars",
    component: _eb4eafaa,
    name: "components-avatars"
  }, {
    path: "/components/badge_label",
    component: _98c8bc6e,
    name: "components-badge_label"
  }, {
    path: "/components/base",
    component: _79183730,
    name: "components-base"
  }, {
    path: "/components/breadcrumb",
    component: _1d0d6622,
    name: "components-breadcrumb"
  }, {
    path: "/components/buttons",
    component: _09cc01f2,
    name: "components-buttons"
  }, {
    path: "/components/cards",
    component: _0b43ab14,
    name: "components-cards"
  }, {
    path: "/components/color_palette",
    component: _1823f650,
    name: "components-color_palette"
  }, {
    path: "/components/drop_dropdowns",
    component: _bca2175e,
    name: "components-drop_dropdowns"
  }, {
    path: "/components/fab_buttons",
    component: _5760c43a,
    name: "components-fab_buttons"
  }, {
    path: "/components/filters",
    component: _3d9f196c,
    name: "components-filters"
  }, {
    path: "/components/footer",
    component: _5845fb3a,
    name: "components-footer"
  }, {
    path: "/components/grid",
    component: _1aad0db6,
    name: "components-grid"
  }, {
    path: "/components/height",
    component: _5c6f30c6,
    name: "components-height"
  }, {
    path: "/components/icons",
    component: _7203396a,
    name: "components-icons"
  }, {
    path: "/components/lists",
    component: _5e2e8374,
    name: "components-lists"
  }, {
    path: "/components/masonry",
    component: _df0b8294,
    name: "components-masonry"
  }, {
    path: "/components/modals_dialogs",
    component: _6fecf91e,
    name: "components-modals_dialogs"
  }, {
    path: "/components/notifications",
    component: _5c0d35ce,
    name: "components-notifications"
  }, {
    path: "/components/pagination",
    component: _1263ca39,
    name: "components-pagination"
  }, {
    path: "/components/progress_spinners",
    component: _4e68a606,
    name: "components-progress_spinners"
  }, {
    path: "/components/scrollable",
    component: _a07fdc74,
    name: "components-scrollable"
  }, {
    path: "/components/slider",
    component: _4588ab80,
    name: "components-slider"
  }, {
    path: "/components/sortable",
    component: _1a6a5557,
    name: "components-sortable"
  }, {
    path: "/components/tables",
    component: _aa0de1f8,
    name: "components-tables"
  }, {
    path: "/components/tabs",
    component: _aa9f7546,
    name: "components-tabs"
  }, {
    path: "/components/timeline",
    component: _1bb05120,
    name: "components-timeline"
  }, {
    path: "/components/toolbar",
    component: _444b7e28,
    name: "components-toolbar"
  }, {
    path: "/components/tooltips",
    component: _5a9263a2,
    name: "components-tooltips"
  }, {
    path: "/components/transitions",
    component: _81db75a2,
    name: "components-transitions"
  }, {
    path: "/components/width",
    component: _15c107f7,
    name: "components-width"
  }, {
    path: "/dashboard/v1",
    component: _d1bb7a80,
    name: "dashboard-v1"
  }, {
    path: "/dashboard/v2",
    component: _d19f4b7e,
    name: "dashboard-v2"
  }, {
    path: "/forms/dynamic_fields",
    component: _34373403,
    name: "forms-dynamic_fields"
  }, {
    path: "/forms/regular_elements",
    component: _376a0284,
    name: "forms-regular_elements"
  }, {
    path: "/forms/validation",
    component: _3bacdec3,
    name: "forms-validation"
  }, {
    path: "/forms/wizard",
    component: _5acafbb5,
    name: "forms-wizard",
    children: [{
      path: "step1",
      component: _4fad8f0b,
      name: "forms-wizard-step1"
    }, {
      path: "step2",
      component: _4fbba68c,
      name: "forms-wizard-step2"
    }, {
      path: "step3",
      component: _4fc9be0d,
      name: "forms-wizard-step3"
    }]
  }, {
    path: "/pages/blank",
    component: _1c7af062,
    name: "pages-blank"
  }, {
    path: "/pages/blank_header_expanded",
    component: _0efc43bb,
    name: "pages-blank_header_expanded"
  }, {
    path: "/pages/chat",
    component: _51fc03ad,
    name: "pages-chat"
  }, {
    path: "/pages/contact_list",
    component: _3f149b9c,
    name: "pages-contact_list"
  }, {
    path: "/pages/contact_list_single",
    component: _4b6970b6,
    name: "pages-contact_list_single"
  }, {
    path: "/pages/gallery",
    component: _8b275be6,
    name: "pages-gallery"
  }, {
    path: "/pages/help_faq",
    component: _2ffd6aed,
    name: "pages-help_faq"
  }, {
    path: "/pages/invoices",
    component: _3d48148a,
    children: [{
      path: "",
      component: _3fca03fe,
      name: "pages-invoices"
    }, {
      path: ":id",
      component: _55362434,
      name: "pages-invoices-id"
    }]
  }, {
    path: "/pages/issues",
    component: _88023262,
    name: "pages-issues",
    children: [{
      path: "details",
      component: _058e1d82,
      name: "pages-issues-details",
      children: [{
        path: ":id?",
        component: _7dc04ba6,
        name: "pages-issues-details-id"
      }]
    }, {
      path: "list",
      component: _17a71b8e,
      name: "pages-issues-list"
    }]
  }, {
    path: "/pages/mailbox",
    component: _51c83b22,
    children: [{
      path: "",
      component: _1aca99b2,
      name: "pages-mailbox"
    }, {
      path: "compose",
      component: _4f23d072,
      name: "pages-mailbox-compose"
    }, {
      path: "message/:id?",
      component: _504242dc,
      name: "pages-mailbox-message-id"
    }]
  }, {
    path: "/pages/notes",
    component: _0a727288,
    name: "pages-notes"
  }, {
    path: "/pages/poi_listing",
    component: _2a27544a,
    name: "pages-poi_listing"
  }, {
    path: "/pages/pricing_tables",
    component: _3a2d53da,
    name: "pages-pricing_tables"
  }, {
    path: "/pages/settings",
    component: _3853a878,
    name: "pages-settings"
  }, {
    path: "/pages/task_board",
    component: _5c5a977e,
    name: "pages-task_board"
  }, {
    path: "/pages/user_profile",
    component: _939763ec,
    name: "pages-user_profile"
  }, {
    path: "/plugins/ajax",
    component: _95553cce,
    name: "plugins-ajax"
  }, {
    path: "/plugins/calendar",
    component: _5b44d992,
    name: "plugins-calendar"
  }, {
    path: "/plugins/charts",
    component: _a15e05e4,
    name: "plugins-charts"
  }, {
    path: "/plugins/code_editor",
    component: _18fddb16,
    name: "plugins-code_editor"
  }, {
    path: "/plugins/data_grid",
    component: _3c3f4392,
    name: "plugins-data_grid"
  }, {
    path: "/plugins/datatables",
    component: _021539c8,
    name: "plugins-datatables"
  }, {
    path: "/plugins/diff_tool",
    component: _65b77a2e,
    name: "plugins-diff_tool"
  }, {
    path: "/plugins/gantt_chart",
    component: _f84bad6c,
    name: "plugins-gantt_chart"
  }, {
    path: "/plugins/google_maps",
    component: _2de83cf4,
    name: "plugins-google_maps"
  }, {
    path: "/plugins/idle_timeout",
    component: _4537836f,
    name: "plugins-idle_timeout"
  }, {
    path: "/plugins/image_cropper",
    component: _ee64f000,
    name: "plugins-image_cropper"
  }, {
    path: "/plugins/push_notifications",
    component: _78cac8bc,
    name: "plugins-push_notifications"
  }, {
    path: "/plugins/tour",
    component: _d551845e,
    name: "plugins-tour"
  }, {
    path: "/plugins/tree",
    component: _18053417,
    name: "plugins-tree"
  }, {
    path: "/plugins/vector_maps",
    component: _0938f5ac,
    name: "plugins-vector_maps"
  }, {
    path: "/plugins/vue_good_table",
    component: _05f6cc44,
    name: "plugins-vue_good_table"
  }, {
    path: "/transaction/direct",
    component: _3a80a3a4,
    name: "transaction-direct"
  }, {
    path: "/transaction/midtrans",
    component: _67aade4a,
    name: "transaction-midtrans"
  }, {
    path: "/forms/advanced_elements/checkbox_radio",
    component: _15b70bc4,
    name: "forms-advanced_elements-checkbox_radio"
  }, {
    path: "/forms/advanced_elements/color_picker",
    component: _625ce1ef,
    name: "forms-advanced_elements-color_picker"
  }, {
    path: "/forms/advanced_elements/date_picker",
    component: _344d2c4a,
    name: "forms-advanced_elements-date_picker"
  }, {
    path: "/forms/advanced_elements/date_range_picker",
    component: _4a5f34e8,
    name: "forms-advanced_elements-date_range_picker"
  }, {
    path: "/forms/advanced_elements/inputmask",
    component: _06226d01,
    name: "forms-advanced_elements-inputmask"
  }, {
    path: "/forms/advanced_elements/multiselect",
    component: _76cd3300,
    name: "forms-advanced_elements-multiselect"
  }, {
    path: "/forms/advanced_elements/range_slider",
    component: _322c1aa8,
    name: "forms-advanced_elements-range_slider"
  }, {
    path: "/forms/advanced_elements/rating",
    component: _24c406e2,
    name: "forms-advanced_elements-rating"
  }, {
    path: "/forms/advanced_elements/select2",
    component: _68813ec1,
    name: "forms-advanced_elements-select2"
  }, {
    path: "/forms/advanced_elements/switches",
    component: _5a6d4b32,
    name: "forms-advanced_elements-switches"
  }, {
    path: "/forms/advanced_elements/time_picker",
    component: _5b5cefcb,
    name: "forms-advanced_elements-time_picker"
  }, {
    path: "/forms/examples/advertising_evaluation_form",
    component: _8db79560,
    name: "forms-examples-advertising_evaluation_form"
  }, {
    path: "/forms/examples/booking_form",
    component: _12ad3fd4,
    name: "forms-examples-booking_form"
  }, {
    path: "/forms/examples/car_rental_form",
    component: _4450acd0,
    name: "forms-examples-car_rental_form"
  }, {
    path: "/forms/examples/checkout_form",
    component: _3e51bd21,
    name: "forms-examples-checkout_form"
  }, {
    path: "/forms/examples/contact_forms",
    component: _dea8da58,
    name: "forms-examples-contact_forms"
  }, {
    path: "/forms/examples/job_application_form",
    component: _15ce5c41,
    name: "forms-examples-job_application_form"
  }, {
    path: "/forms/examples/medical_history_form",
    component: _651a43ae,
    name: "forms-examples-medical_history_form"
  }, {
    path: "/forms/examples/registration_form",
    component: _6d3cfd0e,
    name: "forms-examples-registration_form"
  }, {
    path: "/forms/examples/rental_application_form",
    component: _1bdb94dc,
    name: "forms-examples-rental_application_form"
  }, {
    path: "/forms/examples/transaction_feedback_form",
    component: _91f096be,
    name: "forms-examples-transaction_feedback_form"
  }, {
    path: "/forms/wysiwyg/ckeditor",
    component: _2fb6f93e,
    name: "forms-wysiwyg-ckeditor"
  }, {
    path: "/forms/wysiwyg/quill",
    component: _08a82029,
    name: "forms-wysiwyg-quill"
  }, {
    path: "/",
    component: _56974b58,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
