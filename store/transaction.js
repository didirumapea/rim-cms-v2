// import config from '~/store/config'
let anotherServer = process.env.NODE_ENV === 'development' ? 'http://localhost:3010/api/v2/' : 'https://api.rimember.id/rimv2/'

// STATE AS VARIABLE
export const state = () => ({
	listDirectTransaction: [],
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		// console.log('server init')
	},
	async getListDirectTransfer ({ commit }, payload) {
		return await this.$axios.$get(`transaction/direct/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListMidtrans ({ commit }, payload) {
		return await this.$axios.$get(`transaction/midtrans/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchDirectTransfer ({ commit }, payload) {
		console.log(payload)
		return await this.$axios.$get(`transaction/direct/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchMidtrans ({ commit }, payload) {
		return await this.$axios.$get(`transaction/midtrans/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async approveSettle ({ commit }, payload) {
		// console.log(process.env.NODE_ENV)
		let _data = {
			fid_user: payload.fid_user,
			inv_code: payload.inv_code,
			fid_promo: payload.fid_promo
		}
		return await this.$axios.post(`transaction/settle-voucher-direct`, _data)
			.then(async (response) => {

				await this.$axios.get(`${anotherServer}payment/clear-timeout-direct-settle/inv_code=${_data.inv_code}`)
				// console.log(mRes.data)
				// commit('setlistVoucherUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async approveUnsettle ({ commit }, payload) {
		// console.log(payload)
		let _data = {
			fid_user: payload.fid_user,
			inv_code: payload.inv_code,
			fid_promo: payload.fid_promo
		}
		return await this.$axios.post(`transaction/unsettle-voucher-direct`, _data)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},


}
// MUTATION AS LOGIC
export const mutations = { // syncronous
	setlistVoucherNotUsed(state, payload){
		// console.log(payload)
		state.listVoucherNotUsed = payload
		// this.$cookies.get('banner-top', payload)
	},
	setlistVoucherUsed(state, payload){
		// console.log(payload)
		state.listVoucherUsed = payload
		// this.$cookies.get('banner-top', payload)
	},
	setlistVoucherNotPay(state, payload){
		// console.log(payload)
		state.listVoucherNotPay = payload
		// this.$cookies.get('banner-top', payload)
	},
	useVoucher (state, id) {
		// console.log(state.listVoucherNotUsed.data, id)
		state.listVoucherNotUsed.data = state.listVoucherNotUsed.data.filter(item => item.id !== id)
	},
	voucherTimeExpired (state, id) {
		// console.log(state.listVoucherNotUsed.data, id)
		state.listVoucherNotPay.data = state.listVoucherNotPay.data.filter(item => item.id !== id)
	},
	// SAMPLE
	increment (state, payload) {
		state.count+=payload
	},
	add (state, text) {
		state.list.push({
			text: text,
			done: false
		})
	},
	remove (state, { todo }) {
		state.list.splice(state.list.indexOf(todo), 1)
	},
	toggle (state, todo) {
		todo.done = !todo.done
	}
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},

}
